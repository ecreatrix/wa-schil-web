<?php
	/**
	 * The template for displaying 404 pages (not found)
	 *
	 * @package WordPress
	 * @subpackage Varia
	 *
	 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
	 * @since 1.0.0
	 */
	get_header();
	$header_image = wp_get_attachment_image_src( 230, 'large' );
	if ( $header_image ) {
		$header_image = $header_image[0];
	} else {
		$header_image = ec\Theme\URI . 'assets/images/screenshot.png';
	}
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main responsive-max-width">
			<article id="post-404">
				<div class="entry-content">
					<div class="wp-block-cover alignfull has-background-dim has-custom-content-position is-position-bottom-left" style="background-image:url(<?php echo $header_image ?>)"><div class="wp-block-cover__inner-container">
						<h1 class="page-title"><?php _e( 'That page can&rsquo;t be found.', 'ec-theme' );?></h1>
					</div>
				</div>
			<?php ec\Theme\Content\Cta::get();?>

			<div class="error-404 not-found">
				<header class="page-header">

				</header><!-- .page-header -->
				<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
				<div class="page-content container">
					<p><?php _e( 'It looks like nothing was found at this location.', 'ec-theme' );?></p>
				</div><!-- .page-content -->
				<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
			</div><!-- .error-404 -->
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID();?> -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
