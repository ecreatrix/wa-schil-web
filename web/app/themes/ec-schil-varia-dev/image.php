<?php
	/**
	 * The template for displaying image attachments
	 *
	 * @package WordPress
	 * @subpackage Varia
	 *
	 * @since 1.0.0
	 */
	get_header();
	$header_image = wp_get_attachment_image_src( 230, 'large' );
	if ( $header_image ) {
		$header_image = $header_image[0];
	} else {
		$header_image = ec\Theme\URI . 'assets/images/screenshot.png';
	}
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="entry-content">
				<div class="wp-block-cover alignfull has-background-dim has-custom-content-position is-position-bottom-left" style="background-image:url(<?php echo $header_image ?>)"><div class="wp-block-cover__inner-container">
					<?php the_title( '<h1 class="entry-title">', '</h1>' );?>
				</div>
			</div>
			<?php
				// Start the loop.
				while ( have_posts() ):
					the_post();
				?>

									<article id="post-<?php the_ID();?>"<?php post_class();?>>
										<div class="entry-content container">
											<figure class="entry-attachment wp-block-image">
											<?php
													/**
													 * Filter the default varia image attachment size.
													 *
													 * @since Twenty Sixteen 1.0
													 *
													 * @param string $image_size Image size. Default 'large'.
													 */
													$image_size = apply_filters( 'varia_attachment_size', 'full' );

													echo wp_get_attachment_image( get_the_ID(), $image_size );
												?>

												<figcaption class="wp-caption-text"><?php the_excerpt();?></figcaption>

											</figure><!-- .entry-attachment -->

											<?php
													the_content();
													wp_link_pages(
														[
															'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ec-theme' ) . '</span>',
															'after'       => '</div>',
															'link_before' => '<span>',
															'link_after'  => '</span>',
															'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'ec-theme' ) . ' </span>%',
															'separator'   => '<span class="screen-reader-text">, </span>'
														]
													);
												?>
										</div><!-- .entry-content -->

										<footer class="entry-footer responsive-max-width">
										<?php
												// Retrieve attachment metadata.
												$metadata = wp_get_attachment_metadata();
												if ( $metadata ) {
													printf(
														'<span class="full-size-link"><span class="screen-reader-text">%1$s</span><a href="%2$s">%3$s &times; %4$s</a></span>',
														_x( 'Full size', 'Used before full size attachment link.', 'ec-theme' ),
														esc_url( wp_get_attachment_url() ),
														absint( $metadata['width'] ),
														absint( $metadata['height'] )
													);
												}
											?>

											<nav id="image-navigation" class="navigation image-navigation container">
												<div class="nav-links">
													<div class="nav-previous"><?php previous_image_link( false, __( 'Previous Image', 'ec-theme' ) );?></div>
													<div class="nav-next"><?php next_image_link( false, __( 'Next Image', 'ec-theme' ) );?></div>
												</div><!-- .nav-links -->
											</nav><!-- .image-navigation -->

											<?php varia_entry_footer();?>

										</footer><!-- .entry-footer -->
									</article><!-- #post-## -->

									<?php
											// End the loop.
										endwhile;
									?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php
get_footer();
