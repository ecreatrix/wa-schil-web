<?php
namespace ec\Theme\Wp;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Index::class ) ) {
	class Index {
		public function __construct() {
			add_action( 'after_setup_theme', [$this, 'gutenberg'], 12 );
			add_action( 'after_setup_theme', [$this, 'custom_logo_setup'] );

			remove_action( 'woocommerce_breadcrumb', 10 );

			add_filter( 'login_headerurl', [$this, 'loginURL'] );

			add_action( 'body_class', [$this, 'body_classes'], 10 );
		}

		public function body_classes( $classes ) {
			if ( is_user_logged_in() ) {
				$classes[] = 'user-logged-in';
			} else {
				$classes[] = 'user-not-logged-in';
			}

			return $classes;
		}

		public function custom_logo_setup() {
			$defaults = [
				'height'      => 100,
				'width'       => 400,
				'flex-height' => true,
				'flex-width'  => true,
				'header-text' => ['site-title', 'site-description']
			];

			add_theme_support( 'custom-logo', $defaults );
		}

		public function gutenberg() {
			// Add child theme editor styles, compiled from `style-child-theme-editor.scss`.
			add_editor_style( 'style-editor.css' );

			// Add child theme editor font sizes to match Sass-map variables in `_config-child-theme-deep.scss`.
			add_theme_support(
				'editor-font-sizes',
				[
					[
						'name'      => __( 'Small', 'ec-theme' ),
						'shortName' => __( 'S', 'ec-theme' ),
						'size'      => 15,
						'slug'      => 'small'
					],
					[
						'name'      => __( 'Normal', 'ec-theme' ),
						'shortName' => __( 'M', 'ec-theme' ),
						'size'      => 18,
						'slug'      => 'normal'
					],
					[
						'name'      => __( 'Large', 'ec-theme' ),
						'shortName' => __( 'L', 'ec-theme' ),
						'size'      => 25.92,
						'slug'      => 'large'
					],
					[
						'name'      => __( 'Huge', 'ec-theme' ),
						'shortName' => __( 'XL', 'ec-theme' ),
						'size'      => 31.104,
						'slug'      => 'huge'
					]
				]
			);

			// Add child theme editor color pallete to match Sass-map variables in `_config-child-theme-deep.scss`.
			add_theme_support(
				'editor-color-palette',
				[
					[
						'name'  => __( 'Primary', 'ec-theme' ),
						'slug'  => 'primary',
						'color' => '#ff0000' //red
					],
					[
						'name'  => __( 'Secondary', 'ec-theme' ),
						'slug'  => 'secondary',
						'color' => '#ffce00' //yellow
					],
					[
						'name'  => __( 'Tertiary', 'ec-theme' ),
						'slug'  => 'tertiary',
						'color' => '#43d278' //green
					],
					[
						'name'  => __( 'Quarternary', 'ec-theme' ),
						'slug'  => 'quarternary',
						'color' => '#2b97cc' //blue
					],
					[
						'name'  => __( 'Quintenary', 'ec-theme' ),
						'slug'  => 'quintenary',
						'color' => '#8b47a9' //purple
					],
					[
						'name'  => __( 'Black', 'ec-theme' ),
						'slug'  => 'black',
						'color' => '#000000'
					],
					[
						'name'  => __( 'Light Gray', 'ec-theme' ),
						'slug'  => 'grey-100',
						'color' => '#F8F9FA'
					],
					[
						'name'  => __( 'White', 'ec-theme' ),
						'slug'  => 'white',
						'color' => '#FFFFFF'
					]
				]
			);

			// Enable Full Site Editing
			add_theme_support( 'full-site-editing' );
		}

		public function loginURL() {
			return home_url();
		}
	}
}
