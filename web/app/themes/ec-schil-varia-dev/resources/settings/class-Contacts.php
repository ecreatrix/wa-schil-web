<?php
//Social media, phone number, email, ...
namespace ec\Theme\Settings;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Contacts::class ) ) {
    class Contacts {
        public function __construct() {
            add_shortcode( 'email', [$this, 'email_shortcode'] );
            add_shortcode( 'tel', [$this, 'tel_shortcode'] );
        }

        public static function add_social_media( $parent_class, $link_class = '', $social_links_order = '' ) {
            $settings = Index::get_settings();

            if ( ! $settings ) {
                return '';
            }

            if ( '' === $social_links_order ) {
                $social_links_order = [
                    ['email', 'envelope-square'],
                    ['linkedin', '-square'],
                    ['instagram', ''],
                    ['twitter', '-square'],
                    ['facebook', '-square'],
                    ['youtube', '-square'],
                    ['google-plus', '-square'],
                    ['pinterest', '-square'],
                    ['rss', '-square']
                ];
            }

            $output = [];
            foreach ( $social_links_order as $key ) {
                if ( array_key_exists( $key[0], $settings ) && ! empty( $settings[$key[0]] ) ) {
                    $class = trim( 'nav-link link-' . $key[0] . ' ' . $link_class );

                    $icon = '<i class="fa fa-' . $key[0] . $key[1] . '"></i>';
                    $link = '<a class="' . $class . '" href="' . $settings[$key[0]] . '" target="_blank">' . $icon . '</a>';

                    if ( 'email' === $key[0] ) {
                        $icon = '<i class="fa fa-' . $key[1] . '"></i>';
                        $link = self::email( false, $icon );
                    }

                    $output[] = '<li class="nav-item">' . $link . '</i>';
                }
            }

            $ul_classes = trim( 'social-media ' . $parent_class );

            return '<ul class="' . $ul_classes . '">' . implode( $output ) . '</ul>';
        }

        //Email/tel html 5 links
        public static function email( $email = false, $text = false ) {
            $settings = Index::get_settings();
            if ( ! $email && $settings && array_key_exists( 'email', $settings ) ) {
                $email = $settings['email'];
            }

            if ( ! $text ) {
                $text = $email;
            }

            return '<a href="mailto:' . $email . '">' . $text . '</a>';
        }

        public function email_shortcode() {
            $settings = Index::get_settings();

            if ( ! $settings || ! array_key_exists( 'email', $settings ) ) {
                return '';
            }

            $email = self::email( $settings['email'] );

            return $email;
        }

        public static function tel( $tel = false, $text = false ) {
            $settings = Index::get_settings();
            if ( ! $tell && $settings && array_key_exists( 'tel', $settings ) ) {
                $tel = $settings['tel'];
            }

            if ( ! $text ) {
                $text = $tel;
            }

            return '<a href="tel:1+' . $tel . '">' . $text . '</a>';
        }

        public function tel_shortcode() {
            $settings = Index::get_settings();

            if ( ! $settings || ! array_key_exists( 'tel', $settings ) ) {
                return '';
            }

            $tel = self::tel( $settings['tel'] );

            return $tel;
        }
    }
}
