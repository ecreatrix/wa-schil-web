<?php
namespace ec\Theme\Settings;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Page::class ) ) {
    class Page {
        public function __construct() {
            add_action( 'admin_menu', [$this, 'register_menu'], 99 );
        }

        public function content() {
            echo '<div class="' . Theme\SHORTNAME . '-admin-page ec-admin-page"></div>';

            return '';
        }

        public function register_menu() {
            $admin_page_info = [
                'menu_slug'        => Theme\SHORTNAME,
                'menu_position'    => 2, // Used if theme doesn't have existing menu filter
                'submenu_position' => 2,
                'page_title'       => esc_html__( Theme\NAME, Theme\CODENAME ),
                'menu_title'       => esc_html__( Theme\NAME, Theme\CODENAME ),
                'capability'       => 'manage_options',
                'content_id'       => Theme\SHORTNAME
            ];

            $function = [$this, 'content'];

            add_menu_page( $admin_page_info['page_title'], $admin_page_info['menu_title'], $admin_page_info['capability'], $admin_page_info['menu_slug'], $function, '', $admin_page_info['menu_position'] );
        }
    }
}