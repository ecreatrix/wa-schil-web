<?php
namespace ec\Theme\Settings;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Index::class ) ) {
    class Index {
        public function __construct() {}

        // $key = api name to get api specific settings
        public static function get_options_key( $key = '' ) {
            return Theme\CODENAME . '_settings_' . $key;
        }

        public static function get_settings() {
            $key = self::get_options_key();

            $saved = get_option( $key, [] );

            if ( ! is_array( $saved ) || empty( $saved ) ) { // set options to default if none set
                update_option( $key, [] );
            }

            return wp_parse_args( $saved, [] );
        }

        //Array keys must be whitelisted (IE must be keys of self::$defaults
        public static function save_settings( $new_settings ) {
            $key = self::get_options_key();
            $current_settings = self::get_settings( $key );

            if ( ! is_array( $new_settings ) ) {
                return $current_settings;
            }

            $settings = array_merge( $current_settings, $new_settings );

            $update_successful = update_option( $key, $settings );

            return $update_successful;
        }
    }
}