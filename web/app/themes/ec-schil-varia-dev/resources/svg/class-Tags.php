<?php
namespace ec\Theme\Svg;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Tags::class ) ) {
	class Tags extends \enshrined\svgSanitize\data\AllowedTags {
		/**
		 * Returns an array of tags
		 */
		public static function getTags() {
			/*
			 * var  array Tags that are allowed.
			 */
			return apply_filters( 'svg_allowed_tags', parent::getTags() );
		}
	}
}
