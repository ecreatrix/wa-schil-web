<?php
// https://gist.github.com/JadedDragoon/72866424f26828111e0b8574597f78f3
// https://github.com/darylldoyle/svg-sanitizer
// NOT WORKING
namespace ec\Theme\Svg;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( InsertInline::class ) ) {
	class InsertInline {
		public function __construct() {
			add_filter( 'the_content', [$this, 'svg_inliner'] );
		}

		public function svg_inliner( $content ) {
			if ( '' === $content ) {
				return '';
			}

			$post = new \DOMDocument();
			$indexClass = new Index();
			$sanitizer = $indexClass->get_sanitizer();
			$sanitizer->removeRemoteReferences( true );

			$post->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ) );
			$img_list = $post->getElementsByTagName( 'img' );

			/* regressive loop because http://php.net/manual/en/domnode.replacechild.php#50500 */
			$i = $img_list->length - 1;
			while ( $i > -1 ) {
				$img = $img_list->item( $i );
				$src_url = parse_url( $img->getAttribute( 'src' ), PHP_URL_PATH );
				$src_ext = pathinfo( $src_url, PATHINFO_EXTENSION );

				if ( 'svg' !== $src_ext ) {
					$i--;
					continue;
				}

				// no x-site monkey business
				$svg_host = parse_url( $img->getAttribute( 'src' ), PHP_URL_HOST );
				$this_host = parse_url( get_site_url(), PHP_URL_HOST );

				if ( $this_host !== $svg_host && 'localhost' !== $svg_host ) {
					$i--;
					continue;
				}

				// Use wp-content/ for normal installs and app/ for Bedrock ones
				$wp_pos = strpos( parse_url( $src_url, PHP_URL_PATH ), 'wp-content/', 1 );
				$app_pos = strpos( parse_url( $src_url, PHP_URL_PATH ), 'app/', 1 );

				$pos = $wp_pos ? $wp_pos + 10 : $app_pos + 3;

				$svg_local_path = WP_CONTENT_DIR . substr(
					parse_url( $src_url, PHP_URL_PATH ),
					$pos
				);

				// load the SVG and parse it (and sanitize... obv)
				if ( ! file_exists( $svg_local_path ) ) {
					var_dump( $svg_local_path );
					$i--;
					continue;
				}

				//var_dump( file_get_contents( $svg_local_path ) );
				$clean_svg = $indexClass->sanitize( $svg_local_path );
				if ( ! $clean_svg ) {
					var_dump( $clean_svg );
					$i--;
					continue;
				}

				$svg = new \DOMDocument();
				$svg->loadXML( mb_convert_encoding( $clean_svg, 'HTML-ENTITIES', 'UTF-8' ) );
				print_r( $svg->getElementsByTagName( 'svg' ) );

				continue;

				// replace img with svg
				$img->parentNode->replacechild(
					$post->importNode(
						$svg->getElementsByTagName( 'svg' )->item( 0 ),
						true
					),
					$img
				);

				// inc loop counter
				$i--;
			}

			return $post->saveHTML();
		}
	}
}


