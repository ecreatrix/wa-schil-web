<?php
namespace ec\Theme\Content;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Cta::class ) ) {
    class Cta {
        public function __construct() {}

        // getting all the records of the user by email for the subscription
        public static function check_subscription_status() {
            $user_id = get_current_user_id();

            if ( ! $user_id ) {
                return false;
            }

            $user_info = get_userdata( $user_id );
            $email = $user_info->user_email;
            //var_dump( $email );

            $subscriptions = get_posts( [
                'numberposts' => -1,
                'post_type'   => 'ywsbs_subscription', // Subscription post type
                'orderby'     => 'post_date',          // ordered by date
                'order'       => 'ASC',
                'meta_query'  => [
                    [
                        'key'     => '_billing_email', //additional fields being used to get the data
                        'value'   => $email,
                        'compare' => '='
                    ],
                    [
                        'key'     => '_status',
                        'value'   => 'active', //active subscriptions
                        'compare' => '='
                    ]]
            ] ); // check if user has any active subscription

            foreach ( $subscriptions as $post ) {
                setup_postdata( $post );
                $nextdue = get_post_meta( get_the_id(), '_payment_due_date', true );

                $current_date = date( 'Y-m-d' );

                $date_to_compare = date( 'Y-m-d', strtotime( $nextdue ) );

                if ( strtotime( $date_to_compare ) > strtotime( $current_date ) ) {
                    return 'active'; //returns active
                    break;
                }
            }

            return false;

            wp_reset_postdata();
        }

        public static function get( $version = 'whyschil', $return = false ) {
            if ( ! self::should_show() ) {

                return '';
            }

            $cta = self::version( $version );

            $output = $cta->post_content;
            if ( $return ) {
                return $output;
            }

            echo $output;
        }

        public static function should_show() {
            if ( function_exists( 'is_product' ) && is_product() ) {
                // Already on subcription page so don't show cta
                return false;
            }

            if ( ! is_user_logged_in() ) {
                // If user isn't logged in, then show CTA
                return true;
            }

            $is_account_page = false;
            if ( function_exists( 'is_account_page' ) && is_account_page() ) {
                $is_account_page = true;
            }

            if ( is_page() && ! $is_account_page ) {
                // Pages can choose where to put the CTA manually
                return false;
            }

            $subscription_status = self::check_subscription_status();

            if ( 'active' === $subscription_status ) {
                // Users with a valid subscription don't need to see CTAs
                return false;
            }

            return true;
        }

        public static function version( $version = 'whyschil' ) {
            $joinbar = get_post( 166 );
            $whyschil = get_post( 164 );

            $cta = $whyschil;
            if ( 'join' === $version || 'joinbar' === $version || 'join bar' === $version ) {
                $cta = $joinbar;
            }

            return $cta;
        }
    }
}
