<?php
namespace ec\Theme\Scripts;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Enqueue::class ) ) {
    class Enqueue {
        public function __construct() {
            $this->setup_favicons();

            // General scripts and styles
            add_action( 'wp_enqueue_scripts', [$this, 'project_assets'], 99 );

            // Gutenberg setup
            add_action( 'enqueue_block_editor_assets', [$this, 'blocks_assets'] );

            // Dashboard assets
            add_action( 'admin_enqueue_scripts', [$this, 'dashboard_assets'] );
            add_action( 'login_enqueue_scripts', [$this, 'dashboard_assets'] );
        }

        public function blocks_assets() {
            $this->vendors();

            $scripts = [
                [
                    'filename'     => 'blocks',
                    //'linked_script' => 'blocks',
                    'dependencies' => ['jquery', 'wp-editor', 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-edit-post', 'wp-compose', 'underscore', 'wp-components', Theme\CODENAME . '_vendors']
                    //'parameters'    => self::ajax_parameters(),
                    //'action'        => 'localize'
                ]
            ];

            Index::add_assets( $scripts, 'js' );

            $styles = [
                [
                    'filename' => 'blocks'
                ]
            ];

            Index::add_assets( $styles, 'css' );
        }

        public function bootstrap() {
            $scripts = [
                [
                    'name'     => 'ec-popper',
                    'filename' => 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js'
                ], [
                    'name'         => 'ec-bootstrap',
                    'filename'     => 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js',
                    'dependencies' => ['jquery', 'ec-popper']
                ]
            ];

            Index::add_assets( $scripts, 'js' );
        }

        public function dashboard_assets() {
            $scripts = [
                [
                    'filename'     => 'admin',
                    'dependencies' => ['jquery']
                ]
            ];

            Index::add_assets( $scripts, 'js' );

            $styles = [
                [
                    'filename' => 'admin'
                ]
            ];

            $styles = Index::add_assets( $styles, 'css' );

            wp_enqueue_style( 'wp-color-picker' );

            $this->settings_assets();
            $this->fonts_google();
            $this->fonts_fontawesome( 'style' );
        }

        public function favicon_add( $type = 'colour' ) {
            $output = '';

            $favicon = '/favicon.png';
            if ( 'bw' === $type ) {
                $favicon = '/favicon-bw.png';
            }

            $output .= '<link rel="icon" type="image/png" sizes="32x32" href="' . Theme\URI . 'assets/images' . $favicon . '">';

            return $output;
        }

        public function favicon_add_admin() {
            echo $this->favicon_add( 'bw' );
        }

        public function favicon_add_main() {
            echo $this->favicon_add( 'colour' );
        }

        public function fonts_fontawesome( $load_type = 'script' ) {
            if ( 'style' === $load_type ) {
                // Loads webfont version
                $link = 'https://kit.fontawesome.com/9e2a71fbfe.js';
            } else {
                // Loads SVG version
                $link = 'https://kit.fontawesome.com/d86202f74c.js';
            }

            $fonts = [
                [
                    'name'     => 'ec-fontawesome',
                    'filename' => $link
                ]
            ];

            Index::add_assets( $fonts, 'js' );
        }

        public function fonts_google() {
            $fonts = [
                [
                    'name'     => 'ec-google-fonts',
                    'filename' => 'https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Oswald:wght@400&display=swap'
                ]
            ];

            Index::add_assets( $fonts, 'css' );
        }

        public function project_assets() {
            $scripts = [
                [
                    'filename'     => 'main',
                    'localize'     => 'ec_theme_ajax',
                    'dependencies' => ['jquery', 'ec-bootstrap'],
                    'parameters'   => $this->project_assets_parameters(),
                    'action'       => 'localize'
                ]
            ];

            Index::add_assets( $scripts, 'js' );
            $this->bootstrap();
            $this->fonts_google();
            //$this->fonts_fontawesome();
            $this->fonts_fontawesome( 'style' );

            $styles = [
                [
                    'filename' => 'main'
                ]
            ];

            Index::add_assets( $styles, 'css' );
        }

        function project_assets_parameters() {
            //$args = Theme\Content\Preview::get_args( $attributes );
            //$custom_query = new \WP_Query( $args );

            $parameters = [
                'ajax_url'       => admin_url( 'admin-ajax.php' ),
                'nonce'          => wp_create_nonce( Theme\CODENAME . '_nonce' ),
                'posts_per_page' => get_option( 'posts_per_page' ),
                //'root'           => esc_url_raw( rest_url() ),
                //'posts'          => json_encode( $custom_query->query_vars ), // everything about your loop is here
                //'route'          => Theme\CODENAME . '/v1'
                'current_page'   => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1
                //'max_page'       => $custom_query->max_num_pages
            ];
            wp_reset_postdata(); // reset the query

            return $parameters;
        }

        public function settings_assets() {
            if ( ! function_exists( 'get_current_screen' ) ) {
                return;
            }

            $screen = \get_current_screen();

            if ( 'toplevel_page_' . Theme\SHORTNAME !== $screen->id ) { // only load settings assets on settings page
                return;
            }
            $this->vendors();

            $parameters = [
                'currentPage'    => Theme\Settings\Rest::get_admin_page(),
                'ajax_url'       => admin_url( 'admin-ajax.php' ),
                'nonce'          => wp_create_nonce( Theme\CODENAME . '_nonce' ),

                'admin'          => get_user_meta( get_current_user_id(), Theme\CODENAME, true ),
                'root'           => esc_url_raw( rest_url() ),
                'route'          => Theme\CODENAME . '/v1',
                //'per_page'       => get_option( 'posts_per_page' ),
                'settings'       => Theme\Settings\Index::get_settings(),
                'contact_fields' => Theme\Settings\Rest::contact_form_fields()
            ];

            $scripts = [
                [
                    'filename'     => 'settings',
                    'localize'     => 'ec_theme_ajax',
                    'dependencies' => ['jquery', 'wp-editor', 'wp-i18n', 'wp-element', 'wp-edit-post', 'wp-compose', 'underscore', 'wp-components', Theme\CODENAME . '_vendors', 'wp-api-request', 'wp-api-fetch'],
                    'parameters'   => $parameters,
                    'action'       => 'localize'
                ]
            ];

            Index::add_assets( $scripts, 'js' );

            $styles = [
                [
                    'filename' => 'settings'
                ]
            ];
            Index::add_assets( $styles, 'css' );
        }

        // Favicons
        public function setup_favicons() {
            add_action( 'wp_head', [$this, 'favicon_add_main'] );
            add_action( 'login_head', [$this, 'favicon_add_admin'] );
            add_action( 'admin_head', [$this, 'favicon_add_admin'] );
        }

        public function vendors() {
            $scripts = [
                [
                    'filename'     => 'vendors',
                    'dependencies' => ['jquery']
                ]
            ];

            Index::add_assets( $scripts, 'js' );
        }
    }
}
