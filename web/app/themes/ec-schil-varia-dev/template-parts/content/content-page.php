<?php
	/**
	 * Template part for displaying page content in page.php
	 *
	 * @package WordPress
	 * @subpackage Varia
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 * @since 1.0.0
	 */
?>

<article id="post-<?php the_ID();?>"<?php post_class();?>>
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages(
				[
					'before' => '<div class="page-links">' . __( 'Pages:', 'varia' ),
					'after'  => '</div>'
				]
			);
		?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID();?> -->
