<?php
	/**
	 * Template part for displaying posts
	 *
	 * @package WordPress
	 * @subpackage Varia
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 * @since 1.0.0
	 */
?>

<article id="post-<?php the_ID();?>"<?php post_class();?>>
<?php
	ec\Theme\Posts\Blog::get_kids_bg();
	//ec\Theme\Content\Cta::get();

	echo '<div class="jumbotron"><div class="container">';
	echo '<header class="entry-header responsive-max-width">';
	the_title( '<h1 class="entry-title">', '</h1>' );
	if ( ! is_page() ) {
		echo '<div class="entry-meta">';
		ec\Theme\Posts\Blog::show_categories();
		echo '</div><!-- .meta-info -->';
	}
	echo '</header>';

	//varia_post_thumbnail();

	echo '<div class="entry-content">';
	the_content(
		sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'varia' ),
				[
					'span' => [
						'class' => []
					]
				]
			),
			get_the_title()
		)
	);
	$id = get_the_id();

	echo ec\Theme\Posts\Blog::embed_pdf( $id );

	wp_link_pages(
		[
			'before' => '<div class="page-links">' . __( 'Pages:', 'varia' ),
			'after'  => '</div>'
		]
	);
	echo '</div><!-- .entry-content -->';

	//echo '<footer class="entry-footer responsive-max-width">';
	//varia_entry_footer();
	//echo '</footer><!-- .entry-footer -->';

	if ( ! is_singular( 'attachment' ) ) {
		get_template_part( 'template-parts/post/author', 'bio' );
	}

echo '</div></div></article><!-- #post-${ID} -->';
