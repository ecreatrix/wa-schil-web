/* jshint -W079 */
$ = jQuery.noConflict();

$( document ).on( 'ready', function() {
	$( 'p > img' ).unwrap();
	$( 'p:empty' ).remove();
} );

// Make space for WP adminbar bar when logged in
function setAdminbarHeight( adminbar ) {
	adminbarHeight = adminbar.outerHeight() + 10;

	var nav = $( '#masthead > nav' );
	var content = $( '#main article > div:first-child > div:first-child' );

	nav.css( 'padding-top', adminbarHeight );
	//content.css( 'padding-top', nav.height() );
}

$( window ).on( "orientationchange load resize", function() {
	var adminbar = $( '#wpadminbar' );
	
	if( adminbar && adminbar.length != 0 ) {
		setAdminbarHeight( adminbar )
	}

	checkOffset();
} );

//$( '#collapsing-navbar' ).removeClass( 'show' );
//$( '#collapsing-navbar' ).collapse( 'hide' );
$( 'textarea' ).addClass( 'form-control' );
$( 'input' ).addClass( 'form-control' );

$ = jQuery.noConflict();

function make_header_room() {
	if ( $( 'body.article-make-room' ).length ) {
		var top_navbar_height = parseInt( $( 'header nav' ).outerHeight(), 10 );
		var top_navbar_collapsed_height = 0;

		if ( $( 'header nav .menues.in' ).length ) {
			top_navbar_collapsed_height = parseInt( $( 'header nav .menues.in' ).outerHeight(), 10 );
		}

		top_navbar_height = top_navbar_height - top_navbar_collapsed_height;

		if ( $( 'body.first-child' ).length ) {
			$( '.site-content > article > div:not(.hero-carousel) > div:first-child' ).css( 'padding-top', top_navbar_height );
		}
	}
}

function checkOffset() {
	if ( $( document ).scrollTop() >= 50 ) {
		//scrolled down so add body class for css changes
		$( 'body' ).addClass( 'scrolled' );
	} else if ( $( document ).scrollTop() < 50 ) {
		//scrolled to the top so remove body class for css changes
		$( 'body' ).removeClass( 'scrolled' );
	}
}

function Utils() {}
Utils.prototype = {
	constructor: Utils,
	isElementInView: function( element, fullyInView ) {
		var pageTop = $( window ).scrollTop();
		var pageBottom = pageTop + $( window ).height();
		var elementTop = $( element ).offset().top;
		var elementBottom = elementTop + $( element ).height();
		if ( fullyInView === true ) {
			return ( ( pageTop < elementTop ) && ( pageBottom > elementBottom ) );
		} else {
			return ( ( elementTop <= pageBottom ) && ( elementBottom >= pageTop ) );
		}
	}
};
var Utils = new Utils();

function keep_track_banner() {
	var $second = $( 'article .jumbotron.hero-banner:not(.footer):first-child' )

	if ( $second.length ) {
		var inview = Utils.isElementInView( $second, false );

		if ( inview ) {
			$( 'body' ).removeClass( 'hero-not-in-view' )
		} else {
			$( 'body' ).addClass( 'hero-not-in-view' )
		}
	}
}


$( window ).on( "orientationchange load resize scroll", function() {
	checkOffset();
	keep_track_banner()
} );

// Add bg to header if no top image on page
$( 'body' ).addClass( 'article-make-room' );

if ( !$( '.hero-banner' ).length ) {
	$( 'body' ).addClass( 'hero-not-in-view no-hero first-child' );
}

// Add bg to header if no top image on page
if ( $( 'article > div > .hero-carousel:first-child' ).length || $( 'article > div > .slick-add:first-child' ).length ) {
	$( 'body' ).addClass( 'hero-carousel first-slide' );
}

make_header_room();

$( window ).on( "orientationchange load resize", function() {
	$( '#masthead .menues' ).collapse( 'hide' );
	$( '#masthead .navbar-toggler' ).collapse( 'hide' );

	$( 'body' ).removeClass( 'menu-shown' );

	setTimeout( function() {
		make_header_room();
	}, 100 );
} );

$( document ).ready( function() {
	// Hide menu when clicked anywhere on body
	$( document ).on( 'click', 'body.menu-shown article', function() {
		$( '.menues' ).collapse( 'hide' );
	} );

	// Hide menu when clicked anywhere on body
	$( document ).on( 'click', '.menues .nav-link', function() {
		$( '.menues' ).collapse( 'hide' );
	} );
} );

// Menu has been opened
$( '.menues' ).on( 'show.bs.collapse', function() {
	$( 'body' ).addClass( 'menu-shown' ).addClass( 'menu-collapsing' );
} );
$( '.menues' ).on( 'shown.bs.collapse', function() {
	$( 'body' ).removeClass( 'menu-collapsing' );
} );

$( '.menues' ).on( 'hide.bs.collapse', function() {
	$( 'body' ).removeClass( 'menu-shown' ).addClass( 'menu-collapsing' );
} );
$( '.menues' ).on( 'hidden.bs.collapse', function() {
	$( 'body' ).removeClass( 'menu-collapsing' );
} );

$( 'article div.jumbotron:not(.hero-banner):not(.hero-image):not(.hero-carousel):first' ).attr( 'id', 'after-hero' )
jQuery( function($) { // use jQuery code inside this to avoid "$ is not defined" error
	var current_page = 2;
	var total = $('.wp-block-theme-preview .posts.row').data('pages');
	var button = $('.ec-loadmore-btn');

	function Utils() {}
	Utils.prototype = {
		constructor: Utils,
		isElementInView: function( element, fullyInView ) {
			var pageTop = $( window ).scrollTop();
			var pageBottom = pageTop + $( window ).height();
			var elementTop = $( element ).offset().top;
			var elementBottom = elementTop + $( element ).height();
			if ( fullyInView === true ) {
				return ( ( pageTop < elementTop ) && ( pageBottom > elementBottom ) );
			} else {
				return ( ( elementTop <= pageBottom ) && ( elementBottom >= pageTop ) );
			}
		}
	};
	var Utils = new Utils();

	function show_more() {
		var inview = Utils.isElementInView(button, false );
		if ( inview ) {
			// Load more whenever button is in view
	 		var data = $('.wp-block-theme-preview .page.page-'+current_page)
			if( data.length > 0 ) { 
				//var posts = $(data[0]).children()
				//$('.wp-block-theme-preview .posts.row').append(posts)
				
				//button.text( 'More posts' ); // insert new posts
				current_page++;

				data.fadeIn();

				//data.remove()
			} 

			if (current_page >= total ) {
				// if no data, or on last page then remove the button 
				button.remove(); 
			}
		}
	}

	$( window ).on( "scroll", function() {
		if( button.length > 0 ) {
			show_more();
		}
	} );
});
$ = jQuery.noConflict();

// Reverse Star ratings for comments
$( 'body' ).on( 'init', '#rating', function() {
	$( '#rating' )
		.hide()
		.before(
			'<p class="stars reverse">\
				<span>\
					<a class="star-5" href="#">5</a>\
					<a class="star-4" href="#">4</a>\
					<a class="star-3" href="#">3</a>\
					<a class="star-2" href="#">2</a>\
					<a class="star-1" href="#">1</a>\
				</span>\
			</p>'
		);
} )
// Method smooth scrolls to given anchor point
/*function smoothScrollTo( $anchor ) {
	var $duration = 400; //time (milliseconds) it takes to reach anchor point

	if ( !$( $anchor ).length ) {
		return
	}

	var anchor_location = $( $anchor ).offset().top;

	if ( is_mobile() ) {
		anchor_location -= 62;
	} else if ( $( 'body' ).length ) {
		if ( $( 'body.fixed-navbar' ).length ) {
			anchor_location -= $( 'header#masthead nav.navbar' ).outerHeight();
		}

		if ( $( 'body.fixed-navbar .dropdown-menu .container' ).length ) {
			var first_link = $( 'body .dropdown-menu .container .nav-link:first-child' )[ 0 ].attributes[ 1 ].nodeValue;

			if ( first_link === link.hash ) {
				anchor_location -= $( 'header#masthead nav.navbar' ).outerHeight() + 10;
			}
		}
	}

	$( 'html, body' ).animate( {
		"scrollTop": anchor_location
	}, $duration );
}

//Smooth Scroll
// to top right away
if ( window.location.hash ) {
	scroll( 0, 0 );
}

setTimeout( function() {
	scroll( 0, 0 );
}, 1 );

var $hashURL = location.hash;

//smooth scroll on anchor jump
$( 'html' ).css( {
	display: 'none'
} );

if ( $hashURL !== "" && $hashURL.length > 1 ) {
	$( "html, body" ).animate( {
		scrollTop: 0
	}, 'fast' );

	setTimeout( function() {
		$( 'html' ).css( {
			display: 'block'
		} );
	}, 30 );

	setTimeout( function() {
		smoothScrollTo( $hashURL );
	}, 1000 );
} else {
	$( 'html' ).css( {
		display: 'block'
	} );
}

//Scroll
$( 'a[class*=nav-]:not([data-toggle="collapse"])[href*="#"]:not([href="#"])' ).click( function( e ) {
	var anchor = $( this ).attr( 'href' );
	var prefix = 'http:';

	var parts = anchor.split( "#" );
	var current_page = location.origin + location.pathname;
	if ( parts[ 0 ] === '' || parts[ 0 ].replace( /^\//, '' ) + '/' === current_page.replace( /^\//, '' ) || parts[ 0 ].replace( /^\//, '' )  === current_page.replace( /^\//, '' ) ) {
		e.preventDefault();

		// Link is on current page. Prevent reload and just scroll
		anchor = '#' + parts[ 1 ];

		smoothScrollTo( anchor );

		// Hide menu when one link is clicked
		$( '#collapsing-navbar' ).collapse('hide');

		return false;
	}
} );*/