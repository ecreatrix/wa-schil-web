<?php
/**
 * Child Theme Functions and definitions
 *
 * @package WordPress
 * @subpackage Shawburn
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @since 1.0.0
 */
namespace ec\Theme;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {exit;}

if ( ! class_exists( Build::class ) ) {
	// Get the theme data.
	$the_theme = wp_get_theme();

	defined( __NAMESPACE__ . '\VERSION' ) || define( __NAMESPACE__ . '\VERSION', $the_theme->get( 'Version' ) );

	defined( __NAMESPACE__ . '\URI' ) || define( __NAMESPACE__ . '\URI', get_stylesheet_directory_uri() . '/' );
	defined( __NAMESPACE__ . '\PATH' ) || define( __NAMESPACE__ . '\PATH', get_stylesheet_directory() . '/' );

	defined( __NAMESPACE__ . '\NAME' ) || define( __NAMESPACE__ . '\NAME', 'Schil' );
	defined( __NAMESPACE__ . '\SHORTNAME' ) || define( __NAMESPACE__ . '\SHORTNAME', 'ec-theme-schil' );
	defined( __NAMESPACE__ . '\CODENAME' ) || define( __NAMESPACE__ . '\CODENAME', 'ec_theme_schil' );

	if ( file_exists( PATH . 'resources/includes/autoload.php' ) ) {
		require_once PATH . 'resources/includes/autoload.php';

		spl_autoload_register( function ( $class ) {
			\ec\loader( $class, PATH . 'resources/', 'ec\\Theme\\' );
		} );
	}

	class Build {
		// Holds the main instance.
		private static $__instance = null;

		public function __construct() {
			$this->init_classes();
		}

		public function init_classes() {
			$this->init_content();
			$this->init_plugins();
			$this->init_scripts();
			$this->init_settings();
		}

		public function init_content() {
			if ( class_exists( Posts\Blog::class ) ) {
				new Posts\Blog();
			}

			if ( class_exists( Content\Footer::class ) ) {
				new Content\Footer();
			}
			if ( class_exists( Content\Navwalker::class ) ) {
				new Content\Navwalker();
			}

			if ( class_exists( Content\Menus::class ) ) {
				new Content\Menus();
			}
			if ( class_exists( Content\Preview::class ) ) {
				new Content\Preview();
			}
			if ( class_exists( Wp\Index::class ) ) {
				new Wp\Index();
			}
		}

		public function init_plugins() {
			if ( class_exists( Plugins\Woocommerce::class ) ) {
				new Plugins\Woocommerce();
			}

			if ( class_exists( SVG\Index::class ) ) {
				new SVG\Index();
			}
		}

		public function init_scripts() {
			if ( class_exists( Scripts\Enqueue::class ) ) {
				new Scripts\Enqueue();
			}
			if ( class_exists( Scripts\Rest::class ) ) {
				new Scripts\Rest();
			}
		}

		public function init_settings() {
			if ( class_exists( Settings\Contacts::class ) ) {
				new Settings\Contacts();
			}
			if ( class_exists( Settings\Page::class ) ) {
				new Settings\Page();
			}
			if ( class_exists( Settings\Rest::class ) ) {
				new Settings\Rest();
			}
		}

		// Get/create the plugin instance.
		public static function instance() {
			if ( empty( self::$__instance ) ) {
				self::$__instance = new Build();
			}

			return self::$__instance;
		}
	}

	Build::instance();
}
