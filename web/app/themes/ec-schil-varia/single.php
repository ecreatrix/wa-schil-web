<?php
	/**
	 * The template for displaying all single posts
	 *
	 * @package WordPress
	 * @subpackage Varia
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
	 * @since 1.0.0
	 */
	get_header();
?>
	<section id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
				while ( have_posts() ):
					the_post();

					get_template_part( 'template-parts/content/content', 'single' );

					if ( is_singular( 'attachment' ) ) {
						// Parent post navigation.
						the_post_navigation(
							[
								/* translators: %s: parent post link */
								'prev_text' => sprintf( __( '<span class="meta-nav">Published in</span><br /><span class="post-title">%s</span>', 'ec-theme' ), '%title' )
							]
						);
					} elseif ( is_singular( 'post' ) ) {
					// Previous/next post navigation.
					$args = wp_parse_args(
						[
							'prev_text'          => '%title',
							'next_text'          => '%title',
							'in_same_term'       => false,
							'excluded_terms'     => '',
							'taxonomy'           => 'category',
							'screen_reader_text' => __( 'Newsletter navigation' ),
							'aria_label'         => __( 'Newsletters' ),
							'class'              => 'post-navigation'
						]
					);

					$previous_text = '<span class="meta-nav" aria-hidden="true">' . __( 'Previous Newsletter', 'ec-theme' ) . '</span> ' .
					'<span class="screen-reader-text">' . __( 'Previous newsletter:', 'ec-theme' ) . '</span> <br/>' .
						'<span class="title">%title</span>';

					$previous = get_previous_post_link(
						'<div class="nav-previous">%link</div>',
						$previous_text,
						$args['in_same_term'],
						$args['excluded_terms'],
						$args['taxonomy']
					);

					$next_text = '<span class="meta-nav" aria-hidden="true">' . __( 'Next Newsletter', 'ec-theme' ) . '</span> ' .
					'<span class="screen-reader-text">' . __( 'Next newsletter:', 'ec-theme' ) . '</span> <br/>' .
						'<span class="title">%title</span>';
					$next = get_next_post_link(
						'<div class="nav-next">%link</div>',
						$next_text,
						$args['in_same_term'],
						$args['excluded_terms'],
						$args['taxonomy']
					);

					$navigation = '';
					// Only add markup if there's somewhere to navigate to.
					if ( $previous || $next ) {
						$navigation = _navigation_markup( $previous . $next, $args['class'], $args['screen_reader_text'], $args['aria_label'] );
					}

					//echo $navigation;
				}

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}

				endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
