<?php
namespace ec\Theme\Posts;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( Blog::class ) ) {
    class Blog {
        public function __construct() {
            add_action( 'admin_menu', [$this, 'change_post_label'] );

            add_action( 'init', [$this, 'edit_taxonomies'] );

            add_action( 'init', [$this, 'remove_supports'], 11 );
        }

        function _show_months() {
            $my_archives = wp_get_archives( [
                'type' => 'monthly',
                //'limit' => 0,
                'echo' => 0
            ] );
            var_dump( $my_archives );

            return $my_archives;
        }

        public function change_post_label() {
            global $menu;
            global $submenu;
            $menu[5][0] = 'Newsletters';
            $submenu['edit.php'][5][0] = 'View all';
            $submenu['edit.php'][10][0] = 'Add New';
        }

        public function edit_taxonomies() {
            $args = [
                //'hierarchical' => false
            ];

            $labels = [
                'name'          => _x( 'Years', 'taxonomy general name', 'ec-theme' ),
                'singular_name' => _x( 'Year', 'taxonomy singular name', 'textdomain' ),
                'search_items'  => __( 'Search Years', 'textdomain' ),
                'all_items'     => __( 'All Years', 'textdomain' ),
                'edit_item'     => __( 'Edit Year', 'textdomain' ),
                'update_item'   => __( 'Update Year', 'textdomain' ),
                'add_new_item'  => __( 'Add New Range', 'textdomain' ),
                'new_item_name' => __( 'New Year Name', 'textdomain' ),
                'menu_name'     => __( 'View Year', 'textdomain' )
            ];
            $year = array_merge( [
                'labels'  => $labels,
                'rewrite' => ['slug' => 'year']
            ], $args );
            register_taxonomy( 'category', 'post', $year );
            //unregister_taxonomy_for_object_type( 'category', 'post' );

            /*$labels = [
            'name'          => _x( 'Months', 'taxonomy general name', 'ec-theme' ),
            'singular_name' => _x( 'Month', 'taxonomy singular name', 'textdomain' ),
            'search_items'  => __( 'Search Months', 'textdomain' ),
            'all_items'     => __( 'All Months', 'textdomain' ),
            'edit_item'     => __( 'Edit Month', 'textdomain' ),
            'update_item'   => __( 'Update Month', 'textdomain' ),
            'add_new_item'  => __( 'Add New Month', 'textdomain' ),
            'new_item_name' => __( 'New Month Name', 'textdomain' ),
            'menu_name'     => __( 'Month', 'textdomain' )
            ];
            $month = array_merge( [
            'labels'  => $labels,
            'rewrite' => ['slug' => 'newsletter/month']
            ], $args );
            register_taxonomy( 'month', 'post', $month );*/
            unregister_taxonomy_for_object_type( 'post_tag', 'post' );
        }

        public static function embed_pdf( $post_id, $title = '', $return = true ) {
            $pdf = get_field( 'file' );
            $output = '';

            if ( $pdf && array_key_exists( 'url', $pdf ) && $pdf['url'] ) {
                $title = get_the_title();
                $image = self::get_pdf_thumb( get_the_id(), $title );

                //$output = do_shortcode( '[pdf-embedder url="' . $pdf['url'] . '" toolbar="top"]' );
                //$output = '<iframe src="' . $pdf['url'] . '"></iframe>';
                $output = '<a class="container" title="SCHIL ' . $title . ' Newsletter" href="' . $pdf['url'] . '">' . $image . '</a>';
                $class = 'logged-in';

                if ( ! is_user_logged_in() ) {
                    $settings = Theme\Settings\Index::get_settings();

                    if ( array_key_exists( 'signupRequest', $settings ) ) {
                        $request = '<p class="request block-editor-block-list__block wp-block has-text-align-center has-quarternary-color has-text-color has-small-font-size wp-block"><em>' . $settings['signupRequest'] . '</em></p>';

                        $class = 'not-logged-in';

                        $output = '<a class="container" href="' . get_permalink( 43 ) . '">' . $image . '</a>' . $request;
                    }
                }

                $output = '<div class="pdf-download ' . $class . '">' . $output . '</div>';

            }

            if ( $return ) {
                return $output;
            }

            echo $output;
        }

        public static function get_hero_block( $return = true ) {
            $block = get_post( 258 );

            $output = apply_filters( 'the_content', $block->post_content );

            if ( $return ) {
                return $output;
            }

            echo $output;
        }

        public static function get_kids_bg( $return = false ) {
            $block = get_post( 197 );

            $output = apply_filters( 'the_content', $block->post_content );

            if ( $return ) {
                return $output;
            }

            echo $output;
        }

        public static function get_pdf_thumb( $post_id, $title = '', $return = true ) {
            $post_meta = get_post_meta( $post_id );

            $pdf_id = false;

            if ( $post_meta && array_key_exists( 'file', $post_meta ) ) {
                // No Pdf set
                $pdf_id = $post_meta['file'][0];
            }

            if ( ! $title || empty( $title ) ) {
                $title = get_the_title( $post_id );
            }

            $img = '<img src="' . Theme\URI . 'assets/images/placeholder.jpg" alt="' . $title . '">';
            $class = 'not-found';

            if ( $pdf_id ) {
                $img = wp_get_attachment_image( $pdf_id, 'full' );
                $class = 'found';
            } else if ( has_post_thumbnail( $post_id ) ) {
                $img = wp_get_attachment_image( $post_id, 'full' );
                $class = 'found';
            }
            $img = '<div class="image ' . $class . '">' . $img . '</div>';

            if ( $return ) {
                return $img;
            }

            echo $img;
        }

        /*public static function post_nav() {
        if ( ! is_singular( 'post' ) ) {
        return;
        }

        $word_limit = 8;
        $title = '';

        $previous = get_previous_post();
        $previous_link = '';
        if ( $previous ) {
        //$title         = ': '.Helpers::string_remove_after_word($previous->post_title, $word_limit, '...', true);
        $previous_link = '<div class="col previous skip"><i class="fa fas fa-chevron-left"></i><div class="link"><a href="' . get_permalink( $previous ) . '">Previous' . $title . '</a></div></div>';
        }

        $next = get_next_post();
        $next_link = '';
        if ( $next ) {
        //$title     =': '.Helpers::string_remove_after_word($next->post_title, $word_limit, '...', true);
        $next_link = '<div class="col next skip"><div class="link"><a href="' . get_permalink( $next ) . '">Next' . $title . '</a></div><i class="fa fas fa-chevron-right"></i></div>';
        }

        return '<nav class="jumbotron bg-grey-100" aria-label="Page navigation"><div class="container"><div class="pagination row">' . $previous_link . $next_link . '</div></div></nav>';
        }*/

        public function remove_supports() {
            remove_post_type_support( 'post', 'thumbnail' );
            remove_post_type_support( 'post', 'excerpt' );

            remove_post_type_support( 'post', 'post-formats' );
        }

        static function show_categories() {
            // Hide author, post date, category and tag text for pages.
            if ( 'post' === get_post_type() ) {
                /* translators: used between list items, there is a space after the comma. */
                $categories_list = get_the_category_list( __( ', ', 'varia' ) );
                if ( $categories_list ) {
                    printf(
                        /* translators: 1: SVG icon. 2: posted in label, only visible to screen readers. 3: list of categories. */
                        '<span class="cat-links">%1$s<span class="screen-reader-text">%2$s</span>%3$s</span>',
                        varia_get_icon_svg( 'archive', 16 ),
                        __( 'Posted in', 'varia' ),
                        $categories_list
                    ); // WPCS: XSS OK.
                }
                /* translators: used between list items, there is a space after the comma. */
                $tags_list = get_the_tag_list( '', __( ', ', 'varia' ) );
                if ( $tags_list ) {
                    printf(
                        /* translators: 1: SVG icon. 2: posted in label, only visible to screen readers. 3: list of tags. */
                        '<span class="tags-links">%1$s<span class="screen-reader-text">%2$s</span>%3$s</span>',
                        varia_get_icon_svg( 'tag', 16 ),
                        __( 'Tags:', 'varia' ),
                        $tags_list
                    ); // WPCS: XSS OK.
                }
            }

            // Edit post link.
            edit_post_link(
                sprintf(
                    wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers. */
                        __( 'Edit <span class="screen-reader-text">%s</span>', 'varia' ),
                        [
                            'span' => [
                                'class' => []
                            ]
                        ]
                    ),
                    get_the_title()
                ),
                '<span class="edit-link">' . varia_get_icon_svg( 'edit', 16 ),
                '</span>'
            );
        }
    }
}
