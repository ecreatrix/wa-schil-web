<?php
namespace ec\Theme\Settings;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Rest::class ) ) {
	class Rest {
		public function __construct() {
			add_action( 'rest_api_init', [$this, 'register_routes_settings'] );
		}

		public static function contact_form_fields() {
			$options = [
				'email'     => [
					'key' => 'email', 'label' => 'Email', 'type' => 'text'
				],

				'address'   => [
					'key' => 'address', 'label' => 'Address', 'type' => 'null', 'as' => 'textarea'
				],

				'tel'       => [
					'key' => 'tel', 'label' => 'Phone Number', 'type' => 'text'
				],

				'instagram' => [
					'key' => 'instagram', 'label' => 'Instagram', 'type' => 'text'
				],

				'facebook'  => [
					'key' => 'facebook', 'label' => 'Facebook', 'type' => 'text'
				]
			];

			return $options;
		}

		public static function get_admin_page() {
			return admin_url( 'admin.php?page=' . $_GET['page'] );
		}

		public static function get_settings( \WP_REST_Request $request ) {
			$settings = Index::get_settings();

			return Theme\Scripts\Rest::response_success( $settings );
		}

		public function register_routes_settings() {
			$namespace = Theme\Scripts\Rest::get_route_parent();
			$rest = new Theme\Scripts\Rest();

			register_rest_route(
				$namespace,
				'/settings/',
				[
					'methods'             => \WP_REST_Server::EDITABLE,
					'callback'            => [$this, 'set_settings'],
					'args'                => [],
					'permission_callback' => [$rest, 'manage_options_permission']
				]
			);

			register_rest_route(
				$namespace,
				'/settings/',
				[
					'methods'             => \WP_REST_Server::READABLE,
					'callback'            => [$this, 'get_settings'],
					'permission_callback' => [$rest, 'manage_options_permission']
				]
			);
		}

		public static function set_settings( \WP_REST_Request $request ) {
			$new_settings = $request->get_param( 'settings' );

			$update_successful = Index::save_settings( $new_settings );
			$settings = Index::get_settings();

			if ( $update_successful ) {
				$message = 'Settings updated';

				return Theme\Scripts\Rest::response_success( $message, 'update_success', $settings );
			} else if ( ! $update_successful ) {
				$message = 'No new data provided, settings do not need to be updated.';

				return Theme\Scripts\Rest::response_success( $message, 'no_new_content', $settings );
			} else {
				$message = 'Failed to update settings.';

				return Theme\Scripts\Rest::response_warning( $message, 'no_settings_update', $settings );
			}
		}
	}
}
