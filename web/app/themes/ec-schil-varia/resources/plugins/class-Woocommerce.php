<?php
namespace ec\Theme\Plugins;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Woocommerce::class ) ) {
	class Woocommerce {
		public function __construct() {
			add_action( 'template_redirect', [$this, 'remove_shop_breadcrumbs'] );

			add_filter( 'woocommerce_add_to_cart_redirect', [$this, 'straight_to_checkout'] );
			add_filter( 'woocommerce_product_single_add_to_cart_text', [$this, 'add_to_cart_text'] );
			add_filter( 'woocommerce_product_add_to_cart_text', [$this, 'add_to_cart_text'] );

			//add_filter( 'woocommerce_product_tabs', [$this, 'remove_extra_product_tabs'], 98 );

			add_filter( 'woocommerce_checkout_fields', [$this, 'checkout_fields_bootstrap_classes'] );
			add_filter( 'variation_attribute_options_args', [$this, 'variable_fields_bootstrap_classes'] );

			add_action( 'woocommerce_after_checkout_validation', [$this, 'checkout_field_process'] );
			add_filter( 'woocommerce_checkout_fields', [$this, 'override_checkout_fields'] );

			$this->init_classes();
		}

		public function add_to_cart_text() {
			return __( 'Subscribe', 'ec-theme' );
		}

		function checkout_field_process() {
			// Check if set, if its not set add an error.
			if ( ! $_POST['order_comments'] ) {
				wc_add_notice( __( 'Please add delivery instructions' ), 'error' );
			}

		}

		public function checkout_fields_bootstrap_classes( $fields ) {
			foreach ( $fields as &$fieldset ) {
				foreach ( $fieldset as &$field ) {
					// if you want to add the form-group class around the label and the input
					$field['class'][] = 'form-group';

					// add form-control to the actual input
					$field['input_class'][] = 'form-control';
				}
			}

			return $fields;
		}

		public function init_classes() {
			if ( class_exists( Woocommerce\Storefront::class ) ) {
				new Woocommerce\Storefront();
			}
			if ( class_exists( Woocommerce\StorefrontTemplateHooks::class ) ) {
				new Woocommerce\StorefrontTemplateHooks();
			}
			if ( class_exists( Woocommerce\MyAccount::class ) ) {
				new Woocommerce\MyAccount();
			}
		}

		function override_checkout_fields( $fields ) {
			$fields['order']['order_comments']['required'] = true;
			$fields['billing']['billing_phone']['required'] = true;
			$settings = Theme\Settings\Index::get_settings();

			if ( $settings && array_key_exists( 'checkoutCommentsLabel', $settings ) ) {
				$fields['order']['order_comments']['label'] = $settings['checkoutCommentsLabel'];
			}
			if ( $settings && array_key_exists( 'checkoutCommentsPlaceholder', $settings ) ) {
				$fields['order']['order_comments']['placeholder'] = $settings['checkoutCommentsPlaceholder'];
			}

			return $fields;
		}

		public function remove_extra_product_tabs( $tabs ) {
			                                        //unset( $tabs['description'] );            // Remove the description tab
			                                        //unset( $tabs['reviews'] );                // Remove the reviews tab
			unset( $tabs['additional_information'] ); // Remove the additional information tab

			return $tabs;
		}

		public function remove_shop_breadcrumbs() {
			//if ( is_shop() ) {
			remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
			//}
		}

		public function straight_to_checkout() {
			$checkouturl = wc_get_checkout_url();

			return $checkouturl;
		}

		public function variable_fields_bootstrap_classes( $args ) {
			var_dump( $args );
			$args['class'] = 'form-control';

			return $args;
		}
	}
}
