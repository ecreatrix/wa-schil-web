<?php
namespace ec\Theme\Plugins\Woocommerce;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( MyAccount::class ) ) {
	class MyAccount {
		public function __construct() {
			add_filter( 'woocommerce_account_menu_items', [$this, 'menu_order'] );
		}

		public function menu_order() {
			$menuOrder = [
				'dashboard'       => __( 'Dashboard', 'woocommerce' ),
				'orders'          => __( 'Subscription', 'woocommerce' ),
				//'downloads'          => __( 'Download', 'woocommerce' ),
				'edit-address'    => __( 'Addresses', 'woocommerce' ),
				'edit-account'    => __( 'Account Details', 'woocommerce' ),
				'customer-logout' => __( 'Logout', 'woocommerce' )
			];

			return $menuOrder;
		}
	}
}
