<?php
namespace ec\Theme\Plugins\Woocommerce;
/*
 * Storefront WooCommerce hooks
 *
 * @package  storefront
 *
 * @since    2.0.0
 */
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( StorefrontTemplateHooks::class ) ) {
	class StorefrontTemplateHooks {
		public function __construct() {
			/*
			 * Layout
			 *
			 * @see  storefront_before_content()
			 * @see  storefront_after_content()
			 * @see  woocommerce_breadcrumb()
			 * @see  storefront_shop_messages()
			 */
			remove_action( 'woocommerce_before_main_content', [$this, 'output_content_wrapper'], 10 );
			remove_action( 'woocommerce_after_main_content', [$this, 'output_content_wrapper_end'], 10 );
			remove_action( 'woocommerce_sidebar', [$this, 'get_sidebar'], 10 );
			remove_action( 'woocommerce_after_shop_loop', [$this, 'pagination'], 10 );
			remove_action( 'woocommerce_before_shop_loop', [$this, 'result_count'], 20 );
			remove_action( 'woocommerce_before_shop_loop', [$this, 'woocommerce_catalog_ordering'], 30 );
			add_action( 'woocommerce_before_main_content', [$this, 'storefront_before_content'], 10 );
			add_action( 'woocommerce_after_main_content', [$this, 'storefront_after_content'], 10 );
			add_action( 'storefront_content_top', [$this, 'storefront_shop_messages'], 15 );

			add_action( 'woocommerce_after_shop_loop', [$this, 'storefront_sorting_wrapper'], 9 );
			add_action( 'woocommerce_after_shop_loop', [$this, 'woocommerce_catalog_ordering'], 10 );
			add_action( 'woocommerce_after_shop_loop', [$this, 'result_count'], 20 );
			add_action( 'woocommerce_after_shop_loop', [$this, 'pagination'], 30 );
			add_action( 'woocommerce_after_shop_loop', [$this, 'storefront_sorting_wrapper_close'], 31 );

			add_action( 'woocommerce_before_shop_loop', [$this, 'storefront_sorting_wrapper'], 9 );
			add_action( 'woocommerce_before_shop_loop', [$this, 'woocommerce_catalog_ordering'], 10 );
			add_action( 'woocommerce_before_shop_loop', [$this, 'result_count'], 20 );
			add_action( 'woocommerce_before_shop_loop', [$this, 'storefront_woocommerce_pagination'], 30 );
			add_action( 'woocommerce_before_shop_loop', [$this, 'storefront_sorting_wrapper_close'], 31 );

			add_action( 'storefront_footer', [$this, 'storefront_handheld_footer_bar'], 999 );

			/*
			 * Products
			 *
			 * @see storefront_edit_post_link()
			 * @see storefront_upsell_display()
			 * @see storefront_single_product_pagination()
			 * @see storefront_sticky_single_add_to_cart()
			 */
			add_action( 'storefront_after_footer', [$this, 'storefront_sticky_single_add_to_cart'], 999 );

			/*
			 * Header
			 *
			 * @see storefront_product_search()
			 * @see storefront_header_cart()
			 */
			add_action( 'storefront_header', [$this, 'storefront_header_cart'], 60 );

			/*
			 * Cart fragment
			 *
			 * @see storefront_cart_link_fragment()
			 */
			add_filter( 'woocommerce_add_to_cart_fragments', [$this, 'storefront_cart_link_fragment'] );
		}

		/**
		 * After Content
		 * Closes the wrapping divs
		 *
		 * @since   1.0.0
		 *
		 * @return void
		 */
		function storefront_after_content() {
			//echo '</main><!-- #main -->';
			//echo '</div><!-- #primary -->';
		}

		/**
		 * Before Content
		 * Wraps all WooCommerce content in wrappers which match the theme markup
		 *
		 * @since   1.0.0
		 *
		 * @return void
		 */
		function storefront_before_content() {
			//echo '<div id="primary" class="content-area">';
			//echo '<main id="main" class="site-main" role="main">';
		}

		/**
		 * Cart Fragments
		 * Ensure cart contents update when products are added to the cart via AJAX
		 *
		 * @param  array $fragments Fragments to refresh via AJAX.
		 * @return array Fragments to refresh via AJAX
		 */
		function storefront_cart_link_fragment( $fragments ) {
			global $woocommerce;

			ob_start();
			storefront_cart_link();
			$fragments['a.cart-contents'] = ob_get_clean();

			ob_start();
			storefront_handheld_footer_bar_cart_link();
			$fragments['a.footer-cart-contents'] = ob_get_clean();

			return $fragments;
		}

		/**
		 * Sorting wrapper
		 *
		 * @since   1.4.3
		 *
		 * @return void
		 */
		function storefront_sorting_wrapper() {
			echo '<div class="storefront-sorting">';
		}

		/**
		 * Sorting wrapper close
		 *
		 * @since   1.4.3
		 *
		 * @return void
		 */
		function storefront_sorting_wrapper_close() {
			echo '</div>';
		}
	}
}