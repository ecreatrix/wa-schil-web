<?php
namespace ec\Theme\Plugins\Woocommerce;
/*
 * Storefront WooCommerce Class
 *
 * @package  storefront
 *
 * @since    2.0.0
 */
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Storefront::class ) ) {
	class Storefront {
		public function __construct() {
			add_action( 'after_setup_theme', [$this, 'setup'] );
			add_filter( 'body_class', [$this, 'woocommerce_body_class'] );
			//add_action( 'wp_enqueue_scripts', [$this, 'woocommerce_scripts'], 20 );
			add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
			add_filter( 'woocommerce_product_thumbnails_columns', [$this, 'thumbnail_columns'] );

			// Integrations.
			add_action( 'storefront_woocommerce_setup', [$this, 'setup_integrations'] );
		}

		/**
		 * Add "Includes" meta to parent cart items.
		 * Displayed only on handheld/mobile screens.
		 *
		 * @since  2.3.4
		 *
		 * @param  array   $group_mode_data Group mode data.
		 * @return array
		 */
		public function bundles_group_mode_options_data( $group_mode_data ) {
			$group_mode_data['parent']['features'][] = 'parent_cart_item_meta';

			return $group_mode_data;
		}

		/**
		 * Query WooCommerce Extension Activation.
		 *
		 * @param  string    $extension Extension class name.
		 * @return boolean
		 */
		public function is_woocommerce_extension_activated( $extension = 'WC_Bookings' ) {
			return class_exists( $extension ) ? true : false;
		}

		/**
		 * Assign styles to individual theme mod.
		 *
		 * @deprecated 2.3.1
		 * @since 2.1.0
		 *
		 * @return void
		 */
		public function set_storefront_style_theme_mods() {
			if ( function_exists( 'wc_deprecated_function' ) ) {
				wc_deprecated_function( __FUNCTION__, '2.3.1' );
			} else {
				_deprecated_function( __FUNCTION__, '2.3.1' );
			}
		}

		/**
		 * Sets up theme defaults and registers support for various WooCommerce features.k
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 *
		 * @since 2.4.0
		 *
		 * @return void
		 */
		public function setup() {
			add_theme_support(
				'woocommerce', apply_filters(
					'storefront_woocommerce_args', [
						'single_image_width'    => 416,
						'thumbnail_image_width' => 324,
						'product_grid'          => [
							'default_columns' => 3,
							'default_rows'    => 4,
							'min_columns'     => 1,
							'max_columns'     => 6,
							'min_rows'        => 1
						]
					]
				)
			);

			add_theme_support( 'wc-product-gallery-zoom' );
			add_theme_support( 'wc-product-gallery-lightbox' );
			add_theme_support( 'wc-product-gallery-slider' );

			/*
			 * Add 'storefront_woocommerce_setup' action.
			 *
			 * @since  2.4.0
			 */
			do_action( 'storefront_woocommerce_setup' );
		}

		/*
		|--------------------------------------------------------------------------
		| Integrations.
		|--------------------------------------------------------------------------
		 */

		/**
		 * Sets up integrations.
		 *
		 * @since  2.3.4
		 *
		 * @return void
		 */
		public function setup_integrations() {
			if ( $this->is_woocommerce_extension_activated( 'WC_Bundles' ) ) {
				//add_filter( 'woocommerce_bundled_table_item_js_enqueued', '__return_true' );
				//add_filter( 'woocommerce_bundles_group_mode_options_data', [$this, 'bundles_group_mode_options_data'] );
			}

			if ( $this->is_woocommerce_extension_activated( 'WC_Composite_Products' ) ) {
				//add_filter( 'woocommerce_composited_table_item_js_enqueued', '__return_true' );
				//add_filter( 'woocommerce_display_composite_container_cart_item_data', '__return_true' );
			}
		}

		/**
		 * Cart Link
		 * Displayed a link to the cart including the number of items present and the cart total
		 *
		 * @since  1.0.0
		 *
		 * @return void
		 */
		function storefront_cart_link() {
			if ( ! storefront_woo_cart_available() ) {
				return;
			}

			echo '<a class="cart-contents" href="' . esc_url( wc_get_cart_url() ) . '" title="' . esc_attr_e( 'View your shopping cart', 'storefront' ) . '">';
			/* translators: %d: number of items in cart */
			echo wp_kses_post( WC()->cart->get_cart_subtotal() );
			echo '<span class="count">';
			wp_kses_data( sprintf( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'storefront' ), WC()->cart->get_cart_contents_count() ) );
			echo '</span>';
			echo '</a>';
		}

		/**
		 * Display Header Cart
		 *
		 * @uses  storefront_is_woocommerce_activated() check if WooCommerce is activated
		 * @since  1.0.0
		 *
		 * @return void
		 */
		function storefront_header_cart() {
			if ( storefront_is_woocommerce_activated() ) {
				if ( is_cart() ) {
					$class = 'current-menu-item';
				} else {
					$class = '';
				}
				echo '<ul id="site-header-cart" class="site-header-cart menu">';
				echo '<li class="' . esc_attr( $class ) . '">';
				storefront_cart_link();
				echo '</li>';
				echo '<li>';
				the_widget( 'WC_Widget_Cart', 'title=' );
				echo '</li>';
				echo '</ul>';
			}
		}

		/**
		 * Storefront shop messages
		 *
		 * @uses    storefront_do_shortcode
		 * @since   1.4.4
		 */
		function storefront_shop_messages() {
			if ( ! is_checkout() ) {
				echo wp_kses_post( storefront_do_shortcode( 'woocommerce_messages' ) );
			}
		}

		/**
		 * Validates whether the Woo Cart instance is available in the request
		 *
		 * @since 2.6.0
		 *
		 * @return bool
		 */
		function storefront_woo_cart_available() {
			$woo = WC();

			return $woo instanceof \WooCommerce && $woo->cart instanceof \WC_Cart;
		}

		/**
		 * Product gallery thumbnail columns
		 *
		 * @since  1.0.0
		 *
		 * @return integer number of columns
		 */
		public function thumbnail_columns() {
			$columns = 4;

			if ( ! is_active_sidebar( 'sidebar-1' ) ) {
				$columns = 5;
			}

			return intval( apply_filters( 'storefront_product_thumbnail_columns', $columns ) );
		}

		/**
		 * Add WooCommerce specific classes to the body tag
		 *
		 * @param  array $classes css classes applied to the body tag.
		 * @return array $classes modified to include 'woocommerce-active' class
		 */
		public function woocommerce_body_class( $classes ) {
			$classes[] = 'woocommerce-active';

			// Remove `no-wc-breadcrumb` body class.
			$key = array_search( 'no-wc-breadcrumb', $classes, true );

			if ( false !== $key ) {
				unset( $classes[$key] );
			}

			return $classes;
		}

		/**
		 * WooCommerce specific scripts & stylesheets
		 *
		 * @since 1.0.0
		 */
		public function woocommerce_scripts() {
			global $storefront_version;

			$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

			wp_enqueue_style( 'storefront-woocommerce-style', get_template_directory_uri() . '/assets/css/woocommerce/woocommerce.css', ['storefront-style', 'storefront-icons'], $storefront_version );
			wp_style_add_data( 'storefront-woocommerce-style', 'rtl', 'replace' );

			wp_register_script( 'storefront-header-cart', get_template_directory_uri() . '/assets/js/woocommerce/header-cart' . $suffix . '.js', [], $storefront_version, true );
			wp_enqueue_script( 'storefront-header-cart' );

			wp_enqueue_script( 'storefront-handheld-footer-bar', get_template_directory_uri() . '/assets/js/footer' . $suffix . '.js', [], $storefront_version, true );

			if ( ! class_exists( 'Storefront_Sticky_Add_to_Cart' ) && is_product() ) {
				wp_register_script( 'storefront-sticky-add-to-cart', get_template_directory_uri() . '/assets/js/sticky-add-to-cart' . $suffix . '.js', [], $storefront_version, true );
			}
		}
	}
}
