<?php
namespace ec\Theme\Scripts;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Rest::class ) ) {
	class Rest {
		public function __construct() {
			add_action( 'rest_api_init', [$this, 'posts'] );
		}

		public function download_field( $object, $field_name, $value ) {
			$id = $object['id'];
			$title = $object['title']['rendered'];

			$thumb = '<figure>' . Theme\Posts\Blog::get_pdf_thumb( $object['id'], $title ) . '</figure>';

			return $thumb;
		}

		public function edit_theme_permission() {
			if ( ! current_user_can( 'edit_theme_options' ) ) {
				return self::response_error( 'user_dont_have_permission', __( 'User don\'t have permissions to change options.', '@@text_domain' ) );
			}

			return true;
		}

		public static function get_route_parent() {
			return Theme\CODENAME . '/v1';
		}

		public static function get_settings( \WP_REST_Request $request ) {
			$settings = Settings::get_settings();

			return self::response_success( json_encode( $settings ) );
		}

		public function manage_options_permission() {
			if ( ! current_user_can( 'manage_options' ) ) {
				return self::response_error( 'user_dont_have_permission', __( 'User don\'t have permissions to change options.', '@@text_domain' ) );
			}

			return true;
		}

		public function posts() {
			register_rest_field( 'post', 'pdf', [
				'get_callback'    => [$this, 'download_field'],
				'update_callback' => null,
				'schema'          => null
			] );
			register_rest_field( 'post', 'request_signup', [
				'get_callback'    => [$this, 'request_signup'],
				'update_callback' => null,
				'schema'          => null
			] );
		}

		public function request_signup( $object, $field_name, $value ) {
			$settings = Theme\Settings\Index::get_settings();

			return $settings['signupRequest'];
		}

		// Error rest.
		public static function response_error( $message, $code = '', $returned = [] ) {
			// Will cause script error and stop script immediately
			return new \WP_REST_Response(
				[
					'message'  => $message,
					'type'     => 'error',
					'code'     => $code,
					'returned' => wp_json_encode( $returned )
				],
				401
			);
		}

		// Success rest.
		public static function response_success( $message, $code = '', $returned = [] ) {
			return new \WP_REST_Response(
				[
					'message'  => $message,
					'type'     => 'success',
					'code'     => $code,
					'returned' => wp_json_encode( $returned )
				],
				200
			);
		}

		// Warning rest.
		public static function response_warning( $message, $code = '', $returned = [] ) {
			return new \WP_REST_Response(
				[
					'message'  => $message,
					'type'     => 'warning',
					'code'     => $code,
					'returned' => wp_json_encode( $returned )
				],
				200
			);
		}

		public static function set_settings( \WP_REST_Request $request ) {
			$new_settings = $request->get_param( 'settings' );

			$update_successful = Settings::save_settings( $new_settings );

			if ( $update_successful ) {
				return self::response_success( $new_settings );
			} else if ( ! $update_successful ) {
				return self::response_success( __( 'No new data provided, settings do not need to be updated.', '@@text_domain' ) );
			} else {
				return self::response_warning( 'no_settings_update', __( 'Failed to update settings.', '@@text_domain' ) );
			}
		}
	}
}
