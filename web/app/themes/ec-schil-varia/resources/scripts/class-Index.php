<?php
namespace ec\Theme\Scripts;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Index::class ) ) {
    class Index {
        public function __construct() {}

        // Enqueue assets
        public static function add_assets( $assets, $ext = false ) {
            if ( ! is_array( $assets ) || empty( $assets ) ) {
                return;
            }

            $default_asset = ['filename' => 'main', 'dependencies' => '', 'action' => false];

            foreach ( $assets as $asset ) {
                $asset = array_merge( $default_asset, $asset );

                if ( ! array_key_exists( 'name', $asset ) ) {
                    $asset['name'] = Theme\CODENAME . '_' . $asset['filename'];
                }

                if ( ! array_key_exists( 'localize', $asset ) ) {
                    $asset['localize'] = 'ec_theme_ajax' . $asset['filename'];
                }

                $file = $asset['filename'];
                if ( ! strpos( $file, 'http' ) && 'js' === $ext ) {
                    $file = 'assets/scripts/' . $file;
                } else if ( ! strpos( $file, 'http' ) && 'css' === $ext ) {
                    $file = 'assets/styles/' . $file;
                }

                // Add full version for dev env
                if ( strpos( $file, 'http' ) === false ) {
                    // Local link

                    $asset['filename'] = $file . '.' . $ext;
                }
                self::add_enqueue( $asset, $ext );

                // Add minimised version for prod env
                if ( strpos( $file, 'http' ) === false ) {
                    // Local link

                    $asset['filename'] = $file . '.min.' . $ext;
                }
                self::add_enqueue( $asset, $ext );
            }
        }

        // Default WP enqueue/register as well as return for local usage
        public static function add_enqueue( $asset, $ext = 'css' ) {
            $version = self::get_file_time( $asset['filename'] );
            $filename = self::get_file_path( $asset['filename'] );

            if ( ! $filename ) {
                return;
            }

            if ( 'localize' === $asset['action'] || 'ajax' === $asset['action'] ) { // AJAX script, add script and localize parameters for that script
                $parameters = [];
                if ( array_key_exists( 'parameters', $asset ) ) {
                    $parameters = $asset['parameters'];
                }

                wp_enqueue_script( $asset['name'], $filename, $asset['dependencies'], $version, true );

                return wp_localize_script( $asset['name'], $asset['localize'], $parameters );

            } else if ( 'css' === $ext || 'min.css' === $ext ) {
                return wp_enqueue_style( $asset['name'], $filename, $asset['dependencies'], $version );
            } else if ( 'js' === $ext || 'min.js' === $ext ) {
                return wp_enqueue_script( $asset['name'], $filename, $asset['dependencies'], $version, true );
            }

            return $filename;
        }

        public static function get_file_path( $file ) {
            if ( strpos( $file, 'http' ) !== false ) {
                // Live link

                return $file;
            } else {
                // Local asset
                $uri = Theme\URI . $file;
                $path = Theme\PATH . $file;

                if ( file_exists( $path ) ) {
                    return $uri;
                } else {
                    //var_dump( 'file not found: ' . $path );

                    return false;
                }
            }
        }

        public static function get_file_time( $file ) {
            $path = Theme\PATH . $file;

            // Get file version. Theme/Theme version for external scripts and file edit time for internal scripts
            $version = Theme\VERSION;

            if ( strpos( $file, 'http' ) !== false ) {
                // Live link
                $version = null;
            } else if ( file_exists( $path ) ) {
                $version = filemtime( $path );
            }

            return $version;
        }
    }
}
