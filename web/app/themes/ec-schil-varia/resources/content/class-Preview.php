<?php
/**
 * Server-side rendering of the `preview` block.
 *
 * @package WordPress
 */
namespace ec\Theme\Content;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Preview::class ) ) {
    class Preview {
        public function __construct() {
            if ( ! function_exists( 'register_block_type' ) ) {
                // Gutenberg is not active.
                return;
            }

            add_action( 'init', [$this, 'register_block'], 20 );
            add_action( 'wp_ajax_your_load_more', 'load_more' );
            add_action( 'wp_ajax_nopriv_your_load_more', 'load_more' );
        }

        static function get_args( $attributes ) {
            $limit = -1;
            if ( $attributes && array_key_exists( 'limitNumber', $attributes ) && $attributes['limitNumber'] && array_key_exists( 'postsToShow', $attributes ) ) {
                $limit = $attributes['postsToShow'];
            }

            $tax_query = 0;
            if ( $attributes && array_key_exists( 'category', $attributes ) ) {
                $tax_query = [
                    [
                        'taxonomy' => 'category',
                        'field'    => 'name',
                        'terms'    => $attributes['category']
                    ]
                ];
            }

            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $args = [
                'posts_per_page'   => $limit,
                //'pagination'       => true,
                'post_status'      => 'publish',
                'post_type'        => 'post',
                'order'            => 'date',
                'orderby'          => 'desc',
                'suppress_filters' => false,
                'tax_query'        => $tax_query
                //'paged'            => $paged
            ];

            return $args;
        }

        static function get_button( $href ) {
            return '<svg class="btn more" width="100%" height="100%" viewBox="0 0 187 53" version="1.1" xmlns="http://www.w3.org/2000/svg"><a href="' . $href . '"><path d="M140.717823,0.00404686442 L140.717823,51.8120051 L28.219039,51.8120051 L2.5740592,31.2660745 C-0.859705256,28.5162301 -0.857681824,23.2897047 2.57810606,20.5418838 L28.2595076,7.10542736e-15 L140.717823,0.00404686442 Z M186.5,38.0729004 C186.5,45.6607712 180.348766,51.8120051 172.758872,51.8140285 L149.873854,51.8140285 L149.873854,0.00607029662 L172.758872,0.00607029662 C180.346743,0.00607029662 186.5,6.15932764 186.5,13.7471984 L186.5,38.0729004 Z" fill="#000000" fill-rule="nonzero"></path><text fill="#FFFFFF"><tspan x="13" y="34.4070143">Take a look</tspan></text></a></svg>';
        }

        static function get_content( $link_start ) {
            $pdf_thumb = Theme\Posts\Blog::get_pdf_thumb( get_the_id() );
            if ( $pdf_thumb ) {
                $pdf_thumb = $link_start . $pdf_thumb . '</a>';
            }

            $title = '<span class="title">' . get_the_title() . '</span>';

            $content = html_entity_decode( get_the_content(), ENT_QUOTES, get_option( 'blog_charset' ) );

            $post_content = sprintf(
                '<div class="content">%1$s</div>',
                wp_kses_post( $content )
            );

            $content = $link_start . $title . $post_content . '</a>';
            $content = '<div class="content">' . $pdf_thumb . $content . '</div>';

            return $content;
        }

        public static function get_heading( $attributes ) {
            if ( ! $attributes || ! array_key_exists( 'headingText', $attributes ) || ! array_key_exists( 'showHeading', $attributes ) || ! $attributes['showHeading'] ) {
                // Don't show heading if attributes not available or set to no
                return;
            }

            $heading = $attributes['headingText'];

            $heading = '<div class="label-container"><h2 class="label">' . $heading . '</h2></div>';

            return $heading;
        }

        public static function get_markup_loop( $custom_query, $heading = '' ) {
            $per_page = get_option( 'posts_per_page' );
            $page_number = 1;
            $count = 1;
            $main_classes = ['post', 'col-12 col-lg-4 col-md-6'];

            if ( $custom_query->have_posts() ) {
                $pages = [];
                while ( $custom_query->have_posts() ) {
                    $post = $custom_query->the_post();

                    $id = get_the_id();
                    $href = get_permalink();
                    $link_start = '<a class="btn btn-link" href="' . $href . '">';

                    $content = self::get_content( $link_start );
                    $button = self::get_button( $href );

                    $item = '<div class="item">' . $content . $button . '</div>';

                    $classes = $main_classes;
                    if ( $count > $per_page ) {
                        // reset count for current page
                        $page_number++;
                        $count = 1;
                    }

                    if ( $page_number > 1 ) {
                        // hide everything after the first page
                        $classes[] = 'hide page page-' . $page_number;
                    }

                    $pages[] = '<div class="newsletter-' . $id . ' ' . implode( ' ', $classes ) . '">' . $item . '</div>';

                    $count++;
                }

                $posts = self::get_markup_page( $pages, $page_number, $custom_query );
                wp_reset_postdata(); // reset the query

                $note = self::get_note();

                return sprintf(
                    '<div class="%1$s">%2$s</div>',
                    esc_attr( 'jumbotron wp-block-theme-preview wp-block' ),
                    '<div class="container">' . $heading . $posts . $note . '</div>'
                );
            }
        }

        public static function get_markup_page( $pages, $page_number, $custom_query ) {
            if ( empty( $pages ) ) {
                $posts = '<div class="posts row">No Newsletters Found</div>';
            } else {
                //$posts = [];
                //foreach ( $page_content as $key => $page ) {
                //$posts[] = implode( $page );
                //}

                //$page_number = count( $page_content ) + 1;

                $posts = '<div class="posts row" data-perpage="' . get_option( 'posts_per_page' ) . '" data-pages="' . $page_number . '">' . implode( $pages ) . '</div>';

                if ( $page_number >= 1 ) {
                    // There's at least one more page, show more button
                    $posts .= '<div class="ec-loadmore-btn">Loading</div>';
                }
            }

            return $posts;
        }

        static function get_note() {
            $settings = Theme\Settings\Index::get_settings();
            $note = '';
            if ( $settings && array_key_exists( 'signupRequest', $settings ) ) {
                $note = '<p class="block-editor-block-list__block wp-block has-text-align-center has-quarternary-color has-text-color has-small-font-size wp-block"><em>' . $settings['signupRequest'] . '</em></p>';
            }
        }

        /**
         * Registers the `core/latest-posts` block on server.
         */
        function register_block() {
            $path = Theme\PATH . 'assets/scripts/blocks/preview/block.json';
            if ( ! file_exists( $path ) ) {
                return;
            }

            $metadata = json_decode( file_get_contents( $path ), true );
            if ( $metadata && is_array( $metadata ) ) {
                register_block_type(
                    $metadata['name'],
                    array_merge( $metadata, [
                        'render_callback' => [$this, 'render_block']
                    ] )
                );
            }
        }

        /**
         * Renders the `preview` block on server.
         *
         * @param  array  $attributes The block attributes.
         * @return string Returns the post content with latest posts added.
         */
        function render_block( $attributes = [] ) {
            $heading = self::get_heading( $attributes );

            $args = self::get_args( $attributes );
            $custom_query = new \WP_Query( $args );

            $item_markup = [];

            return self::get_markup_loop( $custom_query, $heading );
        }
    }
}
