<?php
namespace ec\Theme\Content;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Menus::class ) ) {
    class Menus {
        public function __construct() {
            // Setup top nav settings and footer text
            add_action( 'ec_header', [$this, 'header'], 10 );

            // Remove default WP admin bar css
            add_action( 'header', [$this, 'remove_admin_login_header'] );

            register_nav_menus( $this->register_navs() );
            add_action( 'after_setup_theme', [$this, 'remove_varia_theme_locations'], 20 );

            add_filter( 'wp_nav_menu_objects', [$this, 'append_cart_icon'], 10, 2 );
        }

        public function append_cart_icon( $items, $args ) {
            if ( empty( $items ) || 'account' !== $args->theme_location ) {
                //  Wrong menu, return items

                return $items;
            }
            if ( ! class_exists( 'woocommerce' ) ) {
                // Account menu but WooCommerce is inactive, return nothing

                return '';
            }
            foreach ( $items as $item ) {
                if ( 'Checkout' === $item->title ) {
                    // Change checkout item text to cart icon
                    $cart_item_count = WC()->cart->get_cart_contents_count();
                    $cart_count_span = '';
                    if ( $cart_item_count ) {
                        $cart_count_span = '<span class="fa-stack cart-count"><span class="fa fa-circle-o fa-stack-1x"></span><strong class="fa-stack-1x number">' . $cart_item_count . '</strong></span>';
                        // $cart_count_span = '<span class="fa-stack fa-5x has-badge" data-count="' . $cart_item_count . '<i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i></span>';
                    }

                    $item->title = '<i class="fa fa-shopping-bag"></i>' . $cart_count_span;
                    $item->classes[] = 'checkout';
                }

                if ( 'My Account' === $item->title || 'Account' === $item->title || 'My account' === $item->title || 267 == $item->ID ) {
                    // Switch account link title to login for non-logged in users
                    if ( ! is_user_logged_in() ) {
                        $item->title = 'Login';
                    }
                }

            }

            return $items;
        }

        public function brand() {
            $custom_logo_id = get_theme_mod( 'custom_logo' );

            if ( $custom_logo_id ) {
                $title = wp_get_attachment_image( $custom_logo_id, 'full' );
            } else {
                $title = get_bloginfo( 'name' );
            }

            return '<a class="navbar-brand desktop" href="' . esc_url( home_url( '/' ) ) . '">' . $title . '</a>';
        }

        public function collapse_button() {
            $burger = 'assets/images/burger';
            $created_path = Theme\PATH . $burger;
            $created_link = Theme\URI . $burger;

            if ( file_exists( $created_path . '.svg' ) ) {
                $burger = $created_link . '.svg';
            } else if ( file_exists( $created_path . '.png' ) ) {
                $burger = $created_link . '.png';
            } else {
                $burger = '<i class="fa fa-bars" aria-hidden="true"></i>';
            }
            $burger = '<span class="sr-only" style="display: none;">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>';

            return '<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">' . $burger . '</button>';
        }

        public function header( $theme_location ) {
            $woocommerce_nav = self::nav_create( ['theme_location' => 'account', 'container_id' => 'collapsing-account'] );
            $site_nav = self::nav_create( [] );

            $output = $this->brand() . $this->collapse_button() . '<div class="menues navbar-collapse collapse">' . $woocommerce_nav . $site_nav . '</div>';

            $output = '<nav class="navbar fixed-top navbar-light navbar-expand-lg"><div class="container">' . apply_filters( 'ec_nav', $output ) . '</div></nav>';

            echo $output;
        }

        // Add site links to menu nav items
        // Input array $atts = ['theme_location', 'menu classes', 'container_id', 'container_classes']
        public static function nav_create( $given_atts ) {
            // Make array to use with nav menu wp function
            $default_atts = [
                'container'       => 'div',
                'container_id'    => 'collapsing-navbar',
                'container_class' => '',
                'menu_class'      => 'nav navbar-nav',

                'depth'           => 2,

                'theme_location'  => 'primary',
                'echo'            => false
            ];

            $nav_atts = array_merge( $default_atts, $given_atts );

            // Use walker if available
            if ( class_exists( Navwalker::class ) ) {
                $nav_atts['walker'] = new Navwalker();
            }

            return wp_nav_menu( $nav_atts );
        }

        public function register_navs() {
            $menus = [
                'primary' => __( 'Main', 'ec_theme' ),
                'account' => __( 'Secondary', 'ec_theme' ),
                'footer'  => __( 'Footer', 'ec_theme' )
            ];

            return $menus;
        }

        public function remove_admin_login_header() {
            remove_action( 'wp_head', '_admin_bar_bump_cb' );
        }

        public function remove_varia_theme_locations() {
            unregister_nav_menu( 'menu-1' );
            unregister_nav_menu( 'menu-2' );
            unregister_nav_menu( 'social' );
        }
    }
}
