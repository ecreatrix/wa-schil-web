<?php
namespace ec\Theme\Content;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( Footer::class ) ) {
    class Footer {
        public function __construct() {
            add_action( 'ec_footer', [$this, 'footer_content_print'], 10 );
        }

        public function copyright() {
            return '<div class="text-center copywrite"><span class="copy">&copy; ' . date( 'Y' ) . '</span> <span class="name">' . get_bloginfo( 'name' ) . '</span> <span class="sep">|</span> <span class="tagline">' . get_bloginfo( 'description' ) . '</span></div>';
        }

        public function footer_content_print() {
            $social_media = $this->social_media();
            $menu = $this->menu();
            $copyright = $this->copyright();

            echo '<div class="container"><div class="links col-12 col-md-6 row">' . $social_media . $menu . '</div>' . $copyright . '</div>';
        }

        public function menu() {
            $nav_atts = [
                'container'      => false,
                'menu_class'     => 'navbar nav',
                'theme_location' => 'footer',
                'echo'           => false
            ];

            $menu = Menus::nav_create( $nav_atts );

            return '<div class="menu">' . $menu . '</div>';
        }

        public function social_media() {
            return Theme\Settings\Contacts::add_social_media( 'navbar nav', '' );
        }
    }
}
