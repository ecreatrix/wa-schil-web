<?php
namespace enshrined\svgSanitize\ElementReference;

class Subject {
    /**
     * @var \DOMElement
     */
    protected $element;

    /**
     * @var int
     */
    protected $useNestingLimit;

    /**
     * @var Usage[]
     */
    protected $usecollection = [];

    /**
     * @var Usage[]
     */
    protected $usedInCollection = [];

    /**
     * Subject constructor.
     *
     * @param \DOMElement $element
     * @param int         $useNestingLimit
     */
    public function __construct( \DOMElement $element, $useNestingLimit ) {
        $this->element = $element;
        $this->useNestingLimit = $useNestingLimit;
    }

    /**
     * @param Subject $subject
     */
    public function addUse( Subject $subject ) {
        if ( $subject === $this ) {
            throw new \LogicException( 'Cannot add self usage', 1570713416 );
        }
        $identifier = $subject->getElementId();
        if ( isset( $this->usecollection[$identifier] ) ) {
            $this->usecollection[$identifier]->increment();

            return;
        }
        $this->usecollection[$identifier] = new Usage( $subject );
    }

    /**
     * @param Subject $subject
     */
    public function addUsedIn( Subject $subject ) {
        if ( $subject === $this ) {
            throw new \LogicException( 'Cannot add self as usage', 1570713417 );
        }
        $identifier = $subject->getElementId();
        if ( isset( $this->usedInCollection[$identifier] ) ) {
            $this->usedInCollection[$identifier]->increment();

            return;
        }
        $this->usedInCollection[$identifier] = new Usage( $subject );
    }

    /**
     * Clear the internal arrays (to free up memory as they can get big)
     * and return all the child usages DOMElement's
     *
     * @return array
     */
    public function clearInternalAndGetAffectedElements() {
        $elements = array_map( function ( Usage $usage ) {
            return $usage->getSubject()->getElement();
        }, $this->usecollection );

        $this->usedInCollection = [];
        $this->usecollection = [];

        return $elements;
    }

    /**
     * @param  bool  $accumulated
     * @return int
     */
    public function countUse( $accumulated = false ) {
        $count = 0;
        foreach ( $this->usecollection as $use ) {
            $usecount = $use->getSubject()->countUse();
            $count += $use->getCount() * ( $accumulated ? 1 + $usecount : max( 1, $usecount ) );
        }

        return $count;
    }

    /**
     * @return int
     */
    public function countUsedIn() {
        $count = 0;
        foreach ( $this->usedInCollection as $usedIn ) {
            $count += $usedIn->getCount() * max( 1, $usedIn->getSubject()->countUsedIn() );
        }

        return $count;
    }

    /**
     * @return \DOMElement
     */
    public function getElement() {
        return $this->element;
    }

    /**
     * @return string
     */
    public function getElementId() {
        return $this->element->getAttribute( 'id' );
    }

    /**
     * @param  array                                                $subjects Previously processed subjects
     * @param  int                                                  $level    The current level of nesting.
     * @throws \enshrined\svgSanitize\Exceptions\NestingException
     * @return bool
     */
    public function hasInfiniteLoop( array $subjects = [], $level = 1 ) {
        if ( $level > $this->useNestingLimit ) {
            throw new \enshrined\svgSanitize\Exceptions\NestingException( 'Nesting level too high, aborting', 1570713498, null, $this->getElement() );
        }

        if ( in_array( $this, $subjects, true ) ) {
            return true;
        }
        $subjects[] = $this;
        foreach ( $this->usecollection as $usage ) {
            if ( $usage->getSubject()->hasInfiniteLoop( $subjects, $level + 1 ) ) {
                return true;
            }
        }

        return false;
    }
}