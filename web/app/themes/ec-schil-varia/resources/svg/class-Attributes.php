<?php
namespace ec\Theme\Svg;

use ec\Theme as Theme;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( gAttributes::class ) ) {
	class Attributes extends \enshrined\svgSanitize\data\AllowedAttributes {
		/**
		 * Returns an array of attributes
		 *
		 * @return array
		 */
		public static function getAttributes() {

			/*
			 * var  array Attributes that are allowed.
			 */
			return apply_filters( 'svg_allowed_attributes', parent::getAttributes() );
		}
	}
}
