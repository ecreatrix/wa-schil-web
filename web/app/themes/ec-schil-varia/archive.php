<?php
	/**
	 * The template for displaying archive pages
	 *
	 * @package WordPress
	 * @subpackage Varia
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	 * @since 1.0.0
	 */
	get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ):
				echo ec\Theme\Posts\Blog::get_hero_block();

				// Start the Loop.
				while ( have_posts() ):
					the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					$preview = new ec\Theme\Content\Preview();

					$heading = 'Archive: ' . single_cat_title( '', false );
					echo $preview->render_block( ['heading' => $heading, 'category' => single_cat_title( '', false )] );

					// End the loop.
				endwhile;

				// Previous/next page navigation.
				varia_the_posts_navigation();

				// If no content, include the "No posts found" template.
			else:
				get_template_part( 'template-parts/content/content', 'none' );

			endif;
		?>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
