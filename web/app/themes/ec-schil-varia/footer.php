<?php
	/**
	 * The template for displaying the footer
	 *
	 * Contains the closing of the #content div and all content after.
	 *
	 * @package WordPress
	 * @subpackage Varia
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 * @since 1.0.0
	 */
	ec\Theme\Content\Cta::get( 'join' );
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer jumbotron position-sticky">
		<?php
			do_action( 'ec_footer' );
		?>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer();?>

</body>
</html>
